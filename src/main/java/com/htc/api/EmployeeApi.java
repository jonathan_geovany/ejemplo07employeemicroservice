package com.htc.api;


import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.htc.dto.EmployeeRequest;
import com.htc.dto.EmployeeResponse;
import com.htc.entity.EmployeeEntity;
import com.htc.exception.NotFoundEntityException;
import com.htc.service.EmployeeService;
import com.htc.utils.AppConst;

@RestController
@RequestMapping("/employee")
public class EmployeeApi {
	
	@Autowired 
	private EmployeeService employeeService;
	
	@Autowired
	Mapper mapper;
	 
	@GetMapping("/{id}")
	public EmployeeResponse findById(@PathVariable long id){
		Optional<EmployeeEntity> employee  = employeeService.findById(id);
		if(!employee.isPresent()) throw new NotFoundEntityException("id="+id);
		return mapper.map(employee.get(),EmployeeResponse.class);
	}
	
	@GetMapping("/")
	public List<EmployeeEntity> findAll(){
		return employeeService.findAll();
	}
	
	
	@PostMapping("/")
	public ResponseEntity<Object> saveOrUpdate(@Valid @RequestBody EmployeeRequest employeeRequest){
		EmployeeEntity employeeEntity = mapper.map(employeeRequest, EmployeeEntity.class);
		EmployeeResponse e = mapper.map(employeeService.saveOrUpdate(employeeEntity),EmployeeResponse.class);
	
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(e.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
	    headers.add(AppConst.MSG_DESCRIPTION,AppConst.MSG_CREATED);
		return ResponseEntity.created(location).headers(headers).build();
	}
}
