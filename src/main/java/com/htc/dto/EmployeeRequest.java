package com.htc.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Entrada de datos para crear un nuevo empleado")
public class EmployeeRequest {
	private Long id;
	
	@NotNull
	@Size(min=2, max=30, message="El nombre debe tener entre {min} y {max} caracteres")
	@ApiModelProperty(notes="El nombre debe tener mas de dos caracteres y menos de 30")
	private String name;
	
	@NotNull
	@Pattern(regexp="^[0-9]*$", message="El número de telefono sólo puede tener dígitos")
	@ApiModelProperty(notes="El telefono debe ser numerico")
	private String telephone;
	
	@NotNull
	@PositiveOrZero
	@ApiModelProperty(notes="El salario debe ser mayor o igual a cero")
	private Double salary;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	
	
}
