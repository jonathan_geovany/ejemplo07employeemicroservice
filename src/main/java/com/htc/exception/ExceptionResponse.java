package com.htc.exception;

import java.util.Date;
import java.util.List;

public class ExceptionResponse {

	private String message;
	private String description;
	private Date timestamp;
	private List<String> errors;

	public ExceptionResponse(String message, String description) {
		this.timestamp=new Date();
		this.message = message;
		this.description = description;
	}

	public ExceptionResponse(String message, String description, List<String> errors) {
		super();
		this.timestamp=new Date();
		this.message = message;
		this.description = description;
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}
	public String getDescription() {
		return description;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public List<String> getErrors() {
		return errors;
	}
}
