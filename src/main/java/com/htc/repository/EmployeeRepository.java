package com.htc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.htc.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

}
