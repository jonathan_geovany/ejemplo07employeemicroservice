package com.htc.service;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.entity.EmployeeEntity;
import com.htc.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@PostConstruct
	public void init() {
		saveOrUpdate(new EmployeeEntity(1L,"Jonathan Hernandez","7756-2312",99.0));
		saveOrUpdate(new EmployeeEntity(2L,"Giovanni Vasquez","7012-1234",120.0));
	}
	
	
	public EmployeeEntity saveOrUpdate(EmployeeEntity employee) {
		return employeeRepository.saveAndFlush(employee);
	}
	public List<EmployeeEntity> findAll(){
		return employeeRepository.findAll();
	}
	public Optional<EmployeeEntity> findById(Long id){
		return employeeRepository.findById(id);
	}
}
