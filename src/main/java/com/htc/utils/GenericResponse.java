package com.htc.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
public class GenericResponse {
	
	public <T> ResponseEntity<?> createOkResponse(T  param) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConst.MSG_DESCRIPTION, HttpStatus.OK.toString());
		ResponseEntity<T> response = new ResponseEntity<>(param,HttpStatus.OK);
		return response;
	}
}
